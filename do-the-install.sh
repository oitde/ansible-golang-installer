#!/bin/bash
#
# Install Something on a Debian derivative
#

DEBIAN_FRONTEND=noninteractive
sudo apt-get update 
sudo apt-get install -yq --no-install-recommends \
     wget \
     git \
     emacs \
     vim \
     golang
sudo apt-get clean 
sudo rm -rf /var/lib/apt/lists/*
sudo apt-get autoremove -y

echo "GoLang Dev env install succeeded" > /tmp/rapid_image_complete
